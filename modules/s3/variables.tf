variable "bucket_name" {}

variable "DevOpsName" {}

variable "domain_name" {
  type        = string
  description = "The domain name for the website."
}