provider "aws" {
  region     = "${var.aws_region}"
  //profile = "Acklen"
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }

}


module "bucket1" {
  source          = "./modules/s3"
  DevOpsName      = var.DevOpsName
  bucket_name     = var.bucket_name
  domain_name     = var.domain_name
}

module "bucket2" {
  source          = "./modules/s3"
  DevOpsName      = var.DevOpsName
  bucket_name     = var.bucket_name2
  domain_name     = var.domain_name2
}

module "bucket3" {
  source          = "./modules/s3"
  DevOpsName      = var.DevOpsName
  bucket_name     = var.bucket_name3
  domain_name     = var.domain_name3
}

module "bucket4" {
  source          = "./modules/s3"
  DevOpsName      = var.DevOpsName
  bucket_name     = var.bucket_name4
  domain_name     = var.domain_name4
}


module "cloudfront" {
  source                         = "./modules/cloudfront"
  s3_buckets                    = [
                                    {domain_name = module.bucket1.s3_frontend, origin_id = "${var.bucket_name}", path_pattern = "/bucket1"},
                                    {domain_name = module.bucket2.s3_frontend, origin_id = "${var.bucket_name2}", path_pattern = "/bucket2"},
                                    {domain_name = module.bucket3.s3_frontend, origin_id = "${var.bucket_name3}", path_pattern = "/bucket3"},
                                    {domain_name = module.bucket4.s3_frontend, origin_id = "${var.bucket_name4}", path_pattern = "/bucket4"}
                                  ]
  acm_certificate_arn     = var.acm_certificate_arn
  domain_names              = [var.domain_name, var.domain_name2, var.domain_name3, var.domain_name4]
  DevOpsName                    = var.DevOpsName
}

module "route53-bucket1" {
  source                               = "./modules/route53"
  domain_name                          = var.domain_name
  resource_domain_name                 = module.cloudfront.s3_distribution.domain_name
  resource_hosted_zone                 = module.cloudfront.s3_distribution.hosted_zone_id
  route53_zone_id                      = var.route53_zone_id
}

module "route53-bucket2" {
  source                               = "./modules/route53"
  domain_name                          = var.domain_name2
  resource_domain_name                 = module.cloudfront.s3_distribution.domain_name
  resource_hosted_zone                 = module.cloudfront.s3_distribution.hosted_zone_id
  route53_zone_id                      = var.route53_zone_id
}

module "route53-bucket3" {
  source                               = "./modules/route53"
  domain_name                          = var.domain_name3
  resource_domain_name                 = module.cloudfront.s3_distribution.domain_name
  resource_hosted_zone                 = module.cloudfront.s3_distribution.hosted_zone_id
  route53_zone_id                      = var.route53_zone_id
}

module "route53-bucket4" {
  source                               = "./modules/route53"
  domain_name                          = var.domain_name4
  resource_domain_name                 = module.cloudfront.s3_distribution.domain_name
  resource_hosted_zone                 = module.cloudfront.s3_distribution.hosted_zone_id
  route53_zone_id                      = var.route53_zone_id
}


